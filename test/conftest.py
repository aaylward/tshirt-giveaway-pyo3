import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--runslow", action="store_true", default=False, help="run slow tests"
    )
    parser.addoption(
        "--veryslow", action="store_true", default=False, help="run very slow tests"
    )


def pytest_configure(config):
    config.addinivalue_line("markers", "slow: mark test as slow to run")
    config.addinivalue_line("markers", "vslow: mark test as very slow to run")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--veryslow"):
        return
    elif config.getoption("--runslow"):
        skip_very_slow = pytest.mark.skip(reason="need --veryslow option to run")
        for item in items:
            if "vslow" in item.keywords:
                item.add_marker(skip_very_slow)
        return
    skip_slow = pytest.mark.skip(reason="need --runslow option to run")
    for item in items:
        if "slow" in item.keywords or "vslow" in item.keywords:
            item.add_marker(skip_slow)
