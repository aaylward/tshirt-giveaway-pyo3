import pytest
from tshirt_giveaway_pyo3 import ShirtColor, Inventory

@pytest.fixture
def store():
    return Inventory().stock(
        [ShirtColor().change_color(c) for c in ('blue', 'red', 'blue')])

def test_shirts(store):
    assert store.shirts == [ShirtColor.Blue, ShirtColor.Red, ShirtColor.Blue]

def test_most_stocked(store):
    assert store.most_stocked() == ShirtColor.Blue

def test_giveaway(store):
    assert store.giveaway(None) == ShirtColor.Blue
    assert store.giveaway(ShirtColor.Blue) == ShirtColor.Blue
    assert store.giveaway(ShirtColor.Red) == ShirtColor.Red
