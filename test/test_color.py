import pytest
from tshirt_giveaway_pyo3 import ShirtColor

def test_color():
    assert ShirtColor() == ShirtColor.Blue
    assert ShirtColor().change_color('blue') == ShirtColor.Blue
    assert ShirtColor().change_color('red') == ShirtColor.Red