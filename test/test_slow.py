import pytest

@pytest.mark.slow
def test_slow():
    print('running a slow test')


@pytest.mark.vslow
def test_very_slow():
    print('running a very slow test')
