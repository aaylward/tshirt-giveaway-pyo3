# tshirt-giveaway-pyo3

## Overview

This package is a [PyO3](https://pyo3.rs/) implementation of the T-shirt giveaway code from chapter 13.1 of the [Rust Book](https://doc.rust-lang.org/stable/book/ch13-01-closures.html). It is built with [maturin](https://www.maturin.rs/index.html) . It makes use of [pytest](https://docs.pytest.org/en/7.3.x/) for testing [Sphinx](https://www.sphinx-doc.org/en/master/), [GitLab CI](https://docs.gitlab.com/ee/ci/), and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) for documentation.


## Installation

### From source

```sh
git clone https://gitlab.com/aaylward/tshirt-giveaway-pyo3.git
cd tshirt-giveaway-pyo3
pip install .
```

### Check installation

Check that the installation was successful by running:

```sh
tshirt-giveaway-pyo3 --version
```

See the available arguments by running :zsh:`tshirt-giveaway-pyo3 --help`:

```
usage: tshirt-giveaway-pyo3 [-h] [--version] [--user-preference <Color>]

T-shirt giveaway

options:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  --user-preference <color>
                        preferred shirt color
```
