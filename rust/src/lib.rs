use pyo3::prelude::*;
use pyo3::exceptions::PyValueError;

/// Shirt color
#[pyclass]
#[derive(Debug, PartialEq, Copy, Clone)]
enum ShirtColor {
    Red,
    Blue,
}

#[pymethods]
impl ShirtColor {

    #[new]
    fn new() -> Self { ShirtColor::Blue }

    /// Change the shirt color
    /// 
    /// Parameters
    /// ----------
    /// color : str
    ///     a color, must be 'red' or 'blue'
    /// 
    /// Returns
    /// -------
    /// ShirtColor
    ///     a ShirtColor of the indicated color
    #[pyo3(text_signature = "(color)")]
    fn change_color(&self, color: &str) -> PyResult<ShirtColor> {
        match color {
            "red" => Ok(ShirtColor::Red),
            "blue" => Ok(ShirtColor::Blue),
            _ => Err(PyValueError::new_err("color must be 'red' or 'blue'"))
        }
    }
}

/// Representation of a T-shirt inventory
#[pyclass]
#[derive(Clone)]
struct Inventory {
    #[pyo3(get, set)]
    shirts: Vec<ShirtColor>
}

#[pymethods]
impl Inventory {

    #[new]
    fn new() -> Self {
        Inventory { shirts: Vec::<ShirtColor>::new() }
    }

    /// Stock the inventory
    /// 
    /// Parameters
    /// ----------
    /// stock
    ///     iterable of ShirtColors
    /// 
    /// Returns
    /// -------
    /// Inventory
    ///     inventory with the given stock
    #[pyo3(text_signature = "(stock)")]
    fn stock(&self, stock: Vec<ShirtColor>) -> PyResult<Self> {
        Ok(Inventory { shirts: stock })
    }
    
    /// Give away a shirt
    /// 
    /// Parameters
    /// ----------
    /// user_preference: ShirtColor
    ///     preferred shirt color indicated by user
    /// 
    /// Returns
    /// -------
    /// ShirtColor
    ///     color of the given shirt
    #[pyo3(text_signature = "(user_preference)")]
    fn giveaway(&self, user_preference: Option<ShirtColor>) -> ShirtColor {
        user_preference.unwrap_or_else(|| self.most_stocked())
    }
    
    /// Check which shirt color is currently most stocked
    /// 
    /// Return ShirtColor::Blue if both Red and Blue counts are equal
    /// 
    /// Returns
    /// -------
    /// ShirtColor
    ///     most stocked shirt color
    #[pyo3(text_signature = "()")]
    fn most_stocked(&self) -> ShirtColor {
        let mut num_red: usize = 0;
        let mut num_blue: usize = 0;
        for color in &self.shirts {
            match color {
                ShirtColor::Red => num_red += 1,
                ShirtColor::Blue => num_blue += 1,
            }
        }
        if num_red > num_blue { ShirtColor::Red } else { ShirtColor::Blue }
    }
}

#[pymodule]
fn tshirt_giveaway_rust(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<ShirtColor>()?;
    m.add_class::<Inventory>()?;
    Ok(())
}
