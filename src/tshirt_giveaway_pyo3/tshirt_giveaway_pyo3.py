from argparse import ArgumentParser
from tshirt_giveaway_pyo3.version import __version__
from tshirt_giveaway_pyo3.tshirt_giveaway_rust import ShirtColor, Inventory

def parse_arguments():
    parser = ArgumentParser(description='T-shirt giveaway')
    parser.add_argument('--version', action='version',
                    version='%(prog)s {version}'.format(version=__version__))
    parser.add_argument('--user-preference', metavar='<color>',
                        choices=('blue', 'red'), help='preferred shirt color')
    return parser.parse_args()

def main():
    args = parse_arguments()
    store = Inventory().stock(
        [ShirtColor().change_color(c) for c in ('blue', 'red', 'blue')])
    giveaway = store.giveaway(
        ShirtColor().change_color(args.user_preference) if args.user_preference
        else None)
    print(f'Most stocked: {store.most_stocked()}')
    print(f'The user with preference {args.user_preference} gets {giveaway}')
