Installation
============

.. role:: zsh(code)
   :language: zsh

From source
-----------

.. code-block:: zsh

   git clone https://gitlab.com/aaylward/tshirt-giveaway-pyo3.git
   cd tshirt-giveaway-pyo3
   pip install .

Check installation
------------------

Check that the installation was successful by running:

.. code-block:: none

   tshirt-giveaway-pyo3 --version

See the available arguments by running :zsh:`tshirt-giveaway-pyo3 --help`:

.. code-block:: none

   usage: tshirt-giveaway-pyo3 [-h] [--version] [--user-preference <Color>]

   T-shirt giveaway

   options:
     -h, --help            show this help message and exit
     --version             show program's version number and exit
     --user-preference <color>
                           preferred shirt color
