API reference
=====================

.. autoclass:: tshirt_giveaway_pyo3.ShirtColor
  :members:

.. autoclass:: tshirt_giveaway_pyo3.Inventory
  :members:
