CLI reference
=============

.. role:: zsh(code)
   :language: zsh

.. code-block::  none

   usage: tshirt-giveaway-pyo3 [-h] [--version] [--user-preference <Color>]

   T-shirt giveaway

   options:
     -h, --help            show this help message and exit
     --version             show program's version number and exit
     --user-preference <color>
                           preferred shirt color
