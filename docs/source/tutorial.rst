Tutorial
========

.. role:: zsh(code)
   :language: zsh

.. _giveaway:

Giveaway
--------

Running :zsh:`tshirt-giveaway-pyo3` will check the most stocked shirt color
and perform a giveaway

.. code-block:: zsh

   tshirt-giveaway-pyo3

.. code-block:: none

   Most stocked: ShirtColor.Blue
   The user with preference None gets ShirtColor.Blue

A preferred color can be set with the :zsh:`--user-preference` argument.

.. code-block:: zsh

   tshirt-giveaway-pyo3 --user-preference red

.. code-block:: none

   Most stocked: ShirtColor.Blue
   The user with preference red gets ShirtColor.Red
