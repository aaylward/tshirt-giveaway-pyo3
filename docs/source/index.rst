.. minigrep documentation master file, created by
   sphinx-quickstart on 
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

tshirt-giveaway-pyo3 documentation
==================================

.. role:: zsh(code)
   :language: zsh

Overview
--------

This package is a `PyO3 <https://pyo3.rs/>`_ implementation of the T-shirt giveaway code from chapter 13.1 of the `Rust Book <https://doc.rust-lang.org/stable/book/ch13-01-closures.html>`_ . It is built with `maturin <https://www.maturin.rs/index.html>`_ . It makes use of `pytest <https://docs.pytest.org/en/7.3.x/>`_ for testing `Sphinx <https://www.sphinx-doc.org/en/master/>`_ , `GitLab CI <https://docs.gitlab.com/ee/ci/>`_ , and `GitLab Pages <https://docs.gitlab.com/ee/user/project/pages/>`_ for documentation.

Contents
--------

.. toctree::

   installation
   tutorial
   tests
   cli
   api
