Tests
=====

.. role:: zsh(code)
   :language: zsh

To run tshirt-giveaway-pyo3's unit tests, download the git repository and use :zsh:`pytest`:

.. code-block:: zsh

   git clone https://gitlab.com/aaylward/tshirt-giveaway-pyo3.git
   cd tshirt-giveaway-pyo3
   pytest

The unit tests are divided into three categories: fast, slow, and very slow. By default, only the fast tests are run. You can run the slow tests by adding the :zsh:`--runslow` option:

.. code-block:: zsh

   pytest --runslow

To run the slow and very slow tests, use the :zsh:`--veryslow` option:

.. code-block:: zsh

   pytest --veryslow
